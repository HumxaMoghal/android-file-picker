package com.massarttech.android.filepicker.adapters

interface FileAdapterListener {
    fun onItemSelected()
}